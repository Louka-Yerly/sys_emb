#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Permet d'afficher la température et l'icône du thermomètre
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */

#include "drawer.h"

/**
 * method to initialize different colors of the thermometer
 **/
extern void temperature_init();

/**
 * method to display temperature
 *  @param cur current temperature
 *  @param min minimum temperature
 *  @param max maximum temperature
 **/
extern void display_temp(int cur, int min, int max);

/**
 * method to display the icon of the thermometer
 **/
extern void display_icon();

/**
 * method to set default color values
 **/
extern void default_color();

/**
 * method to change color of current temperature
 * @param color the desired color
 **/
extern void change_color_cur(enum colors color);

/**
 * method to change color of minimum temperature
 * @param color the desired color
 **/
extern void change_color_min(enum colors color);

/**
 * method to change color of maximum temperature
 * @param color the desired color
 **/
extern void change_color_max(enum colors color);

/**
 * method to change color of the thermometer
 * @param color the desired color
 **/
extern void change_color_thermo(enum colors color);

/**
 * method to reset colors
 **/
extern void temperature_reset();
