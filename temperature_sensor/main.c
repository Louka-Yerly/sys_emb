/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              le boutons poussoir de l'encodeur et un écran OLED
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "menu.h"
#include "temperature.h"
#include "thermo.h"
#include "wheel.h"

static const char* banner =
    "HEIA-FR - Embedded Systems 1 Laboratory\n"
    "An introduction the C programming language\n"
    "--> LCD, temperature sensor and wheel demo program\n";

//-- implementation of public methods ---------------------------------------

int main()
{
    // display banner on the console
    printf("%s", banner);

    // initialize the drivers
    wheel_init();
    drawer_init();
    thermo_init();
    menu_init();
    temperature_init();
    display_icon();
    struct thermo temperature = thermo_values();
    display_temp(temperature.cur, temperature.min, temperature.max);

    // main loop
    enum wheel_directions direction = WHEEL_STILL;
    while (true) {
        direction = wheel_direction();

        if (direction == WHEEL_LEFT || direction == WHEEL_RIGHT) {
            display_menu();
            display_icon();
            temperature = thermo_values();
            display_temp(temperature.cur, temperature.min, temperature.max);
        }
    }
    return 0;
}