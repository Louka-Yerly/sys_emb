/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de récupérer des valeur d'un
 *              thermomètre placé sur la carte d'extension
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */

#include "thermo.h"

#include <bbb/am335x_i2c.h>

#define CHIP_ID 0x48
#define REGISTER 0
#define I2C AM335X_I2C2
#define SPEED 400000
#define ERROR_TEMP 100

static int8_t cur_temp;
static int8_t min_temp;
static int8_t max_temp;

void thermo_init()
{
    am335x_i2c_init(I2C, SPEED);
    thermo_reset();
}

struct thermo thermo_values()
{
    return (struct thermo){.cur = cur_temp, .min = min_temp, .max = max_temp};
}

void thermo_update()
{
    uint8_t data[2];
    int status = am335x_i2c_read(I2C, CHIP_ID, REGISTER, data, sizeof(data));

    if (status == 0) {
        if (data[0] > max_temp) max_temp = data[0];
        if (data[0] < min_temp) min_temp = data[0];
        cur_temp = data[0];
    } else {
        cur_temp = ERROR_TEMP;
    }
}

void thermo_reset()
{
    thermo_update();
    min_temp = cur_temp;
    max_temp = cur_temp;
}
