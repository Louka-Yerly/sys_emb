/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de présenter différents menus
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */

#include "menu.h"

#include <stdbool.h>
#include <stdio.h>
#include <string.h>

#include "drawer.h"
#include "temperature.h"
#include "thermo.h"
#include "wheel.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
enum menu { MAIN, COLOR };

static void display(enum menu menu_type);
static void loop_menu(enum menu menu_type);
static bool process_pressed(enum menu menu_type);
static void draw_menu(enum menu menu_type);
static void draw_selected_menu(enum menu menu_type, bool select);
static const char** menu_tab(enum menu menu_type);
static void set_nb_menu(enum menu menu_type);
static bool menu_curr_value();
static bool menu_min_value();
static bool menu_max_value();
static bool menu_thermo();
static bool menu_default();
static bool menu_back();

enum main_menu_item {
    curr_value        = 0,
    min_value         = 1,
    max_value         = 2,
    thermo            = 3,
    default_parameter = 4,
    back              = 5
};

// En faisant comme ça je peux choisir facilement l'emplacement des menus
static const char* menu_main[] = {[curr_value]        = "curr value",
                                  [min_value]         = "min value",
                                  [max_value]         = "max value",
                                  [thermo]            = "thermo",
                                  [default_parameter] = "default",
                                  [back]              = "back"};

static enum colors color_tab[] = {white, black, red, blue, orange, green, pink};
static const char* menu_color[] = {
    "White", "Black", "Red", "Blue", "Orange", "Green", "Pink"};

typedef bool (*function_t)();
function_t menu_functions[] = {menu_curr_value,
                               menu_min_value,
                               menu_max_value,
                               menu_thermo,
                               menu_default,
                               menu_back};

static int position = 0;
static int nb_menu  = 0;

void menu_init()
{
    position = 0;
    nb_menu  = ARRAY_SIZE(menu_main);
}

void display_menu()
{
    display(MAIN);
    loop_menu(MAIN);
}

/**
 * Call the function to draw the menus items
 * @param menu_type allow you to choice the type of menu
 */
static void display(enum menu menu_type)
{
    draw_menu(menu_type);
    position = 0;
    draw_selected_menu(menu_type, true);
}

/**
 * Loop while the exit boolean is set to false
 * @param menu_type allow you to choice the type of menu
 */
static void loop_menu(enum menu menu_type)
{
    enum wheel_directions direction = WHEEL_STILL;
    while (true) {
        direction = wheel_direction();
        if (direction == WHEEL_LEFT || direction == WHEEL_RIGHT) {
            draw_selected_menu(menu_type, false);
            position = direction == WHEEL_LEFT
                           ? (position + nb_menu - 1) % nb_menu
                           : (position + nb_menu + 1) % nb_menu;
            draw_selected_menu(menu_type, true);
        }

        if (wheel_button_state() == WHEEL_PRESSED) {
            if (process_pressed(menu_type)) {
                break;
            }
        }
    }
}

/**
 * Process on_click action according to the menu_type
 * @param menu_type allow you to choice the type of menu
 */
static bool process_pressed(enum menu menu_type)
{
    if (menu_type == MAIN) {
        return menu_functions[position]();
    } else if (menu_type == COLOR) {
        return true;
    } else {
        return true;
    }
}

/**
 * Draw all the menus items
 * @param menu_type allow you to choice the type of menu
 */
static void draw_menu(enum menu menu_type)
{
    drawer_clear();
    set_nb_menu(menu_type);
    const char** menu = menu_tab(menu_type);

    for (int i = 0; i < nb_menu; i++) {
        drawer_string(menu[i], strlen(menu[i]), 8 * i, 0, white, black);
    }
}

/**
 * Draw the selected menu
 * @param menu_type allow you to choice the type of menu
 * @param select allow you to unselect a selected menu or to select an
 * unselected menu
 */
static void draw_selected_menu(enum menu menu_type, bool select)
{
    const char** menu      = menu_tab(menu_type);
    enum colors text       = select ? black : white;
    enum colors background = select ? white : black;

    drawer_rectangle(0, position * 8, 96, 8, background);
    drawer_string(menu[position],
                  strlen(menu[position]),
                  8 * position,
                  0,
                  text,
                  background);
}

/**
 * @param menu_type allow you to choice the type of menu
 * @return The menu array that match with menu_type
 */
static const char** menu_tab(enum menu menu_type)
{
    const char** menu;
    if (menu_type == MAIN)
        menu = menu_main;
    else if (menu_type == COLOR)
        menu = menu_color;
    else
        menu = menu_main;
    return menu;
}

/**
 * Set the number of menu in the variable nb_menu
 * @param menu_type allow you to choice the type of menu
 */
static void set_nb_menu(enum menu menu_type)
{
    if (menu_type == MAIN)
        nb_menu = ARRAY_SIZE(menu_main);
    else if (menu_type == COLOR)
        nb_menu = ARRAY_SIZE(menu_color);
    else
        nb_menu = ARRAY_SIZE(menu_main);
}

/**
 * Change the color of the current temperature
 */
static bool menu_curr_value()
{
    display(COLOR);
    loop_menu(COLOR);
    enum colors curr_color = color_tab[position];
    change_color_cur(curr_color);
    display(MAIN);
    return false;
}

/**
 * Change the color of the min temperature
 */
static bool menu_min_value()
{
    display(COLOR);
    loop_menu(COLOR);
    enum colors min_color = color_tab[position];
    change_color_min(min_color);
    display(MAIN);
    return false;
}

/**
 * Change the color of the max temperature
 */
static bool menu_max_value()
{
    display(COLOR);
    loop_menu(COLOR);
    enum colors max_color = color_tab[position];
    change_color_max(max_color);
    display(MAIN);
    return false;
}

/**
 * Update the the thermo values and leave the menu
 */
static bool menu_thermo()
{
    thermo_update();
    menu_back();
    return true;
}

/**
 * Reset the thermo values, the temperature colors and leave the menu
 */
static bool menu_default()
{
    thermo_reset();
    temperature_reset();
    menu_thermo();
    return true;
}

/**
 * Leave the menu
 */
static bool menu_back()
{
    position = 0;
    drawer_clear();
    return true;
}