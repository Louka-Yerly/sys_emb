/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif
 *              de la carte d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 * Version :    V2
 */
#include "wheel.h"

#include <bbb/am335x_gpio.h>
#include <stdbool.h>

#define CHA_GPIO AM335X_GPIO2
#define CHA 1

#define CHB_GPIO AM335X_GPIO1
#define CHB 29

#define SW_GPIO AM335X_GPIO0
#define SW 2

static const enum wheel_states button_transition[2][2] = {
    {WHEEL_OPEN, WHEEL_PRESSED}, {WHEEL_RELEASED, WHEEL_CLOSED}};

static int former_state_button = 0;

static int former_state_wheel = 0;

/*
 * array of the possible transition. First dimension is new_state and seconde
 * dimension is former_state
 */
static const enum wheel_directions wheel_transition[4][4] = {
    {WHEEL_STILL, WHEEL_STILL, WHEEL_STILL, WHEEL_STILL},
    {WHEEL_RIGHT, WHEEL_STILL, WHEEL_STILL, WHEEL_LEFT},
    {WHEEL_LEFT, WHEEL_STILL, WHEEL_STILL, WHEEL_RIGHT},
    {WHEEL_STILL, WHEEL_STILL, WHEEL_STILL, WHEEL_STILL},
};

// method to get the state of the two GPIO of the wheel
static int get_wheel_state()
{
    int state = 0;
    if (am335x_gpio_get_state(CHA_GPIO, CHA)) state += 1;
    if (am335x_gpio_get_state(CHB_GPIO, CHB)) state += 2;
    return state;
}

static int get_button_state()
{
    int state = 0;
    if (!(am335x_gpio_get_state(SW_GPIO, SW))) state += 1;
    return state;
}

void wheel_init()
{
    am335x_gpio_init(CHA_GPIO);
    am335x_gpio_init(CHB_GPIO);
    am335x_gpio_init(SW_GPIO);

    am335x_gpio_setup_pin_in(CHA_GPIO, CHA, AM335X_GPIO_PULL_UP, true);
    am335x_gpio_setup_pin_in(CHB_GPIO, CHB, AM335X_GPIO_PULL_NONE, true);
    am335x_gpio_setup_pin_in(SW_GPIO, SW, AM335X_GPIO_PULL_NONE, true);

    // Get initial direction
    former_state_wheel  = get_wheel_state();
    former_state_button = get_button_state();
}

enum wheel_directions wheel_direction()
{
    int new_state = get_wheel_state();
    enum wheel_directions direction =
        wheel_transition[new_state][former_state_wheel];
    former_state_wheel = new_state;
    return direction;
}

enum wheel_states wheel_button_state()
{
    int new_state = get_button_state();
    enum wheel_states transition =
        button_transition[former_state_button][new_state];
    former_state_button = new_state;
    return transition;
}
