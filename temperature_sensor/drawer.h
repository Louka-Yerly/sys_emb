#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     CConcevoir un programme permettant de dessiner un cercle, un
 *              rectangle, un charactère et une chaine de charactères (string)
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */

/**
 * The different colors available
 */
enum colors {
    white  = 0xffff,
    black  = 0x0000,
    red    = 0xf800,
    blue   = 0x0000fa,
    orange = 0xd420,
    green  = 0x0420,
    pink   = 0xda6e
};

/**
 * method to initilize the drawer
 **/
extern void drawer_init();

/**
 * Method to draw a single char
 * @param c the char to draw
 * @param x the x-coordinate
 * @param y the y-coordinate
 * @param colorFront the color of the char
 * @param colorBack the color behind the char
 **/
extern void drawer_char(
    char c, int x, int y, enum colors colorFront, enum colors colorBack);

/**
 * Method to draw a string
 * @param str the string to draw
 * @param x the x-coordinate
 * @param y the y-coordinate
 * @param colorFront the color of the char
 * @param colorBack the color behind the char
 **/
extern void drawer_string(const char* str,
                          int length,
                          int x,
                          int y,
                          enum colors colorFront,
                          enum colors colorBack);

/**
 * Method to draw a rectangle
 * @param x the x-coordinate
 * @param y the y-coordinate
 * @param w the with of the rectangle
 * @param h the heigh of the rectangle
 * @param color the color of the rectangle
 **/
extern void drawer_rectangle(int x, int y, int w, int h, enum colors color);

/**
 * Method to draw a circle
 * @param x the x-coordinate
 * @param y the y-coordinate
 * @param r the radius of the circle
 * @param color the color of the circle
 **/
extern void drawer_circle(int x, int y, int r, enum colors color);

/**
 * Method to clear all the screen
 **/
extern void drawer_clear();
