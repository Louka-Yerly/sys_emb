#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif
 *              de la carte d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */

#include <stdint.h>

enum wheel_directions { WHEEL_STILL, WHEEL_LEFT, WHEEL_RIGHT };
enum wheel_states { WHEEL_PRESSED, WHEEL_RELEASED, WHEEL_OPEN, WHEEL_CLOSED };

/**
 * method to initialize the wheel
 */
extern void wheel_init();

/**
 * method to get the
 * @return the actual direction of the wheel
 */
extern enum wheel_directions wheel_direction();

/**
 * method to get the wheel button
 * @return  1=pressed, 0=released
 */
extern enum wheel_states wheel_button_state();
