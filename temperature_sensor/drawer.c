/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme permettant de dessiner un cercle, un
 *              rectangle, un charactère et une chaine de charactères (string)
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */
#include "drawer.h"

#include <bbb/font_8x8.h>
#include <bbb/oled.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

void drawer_init() { oled_init(OLED_V101); }

void drawer_char(
    char c, int col, int row, enum colors color, enum colors colorBack)
{
    struct pixel s[8][8] = {0};
    struct pixel color_1 = {.hword = color >> 8, .lword = color & 0xff};
    struct pixel color_0 = {.hword = colorBack >> 8, .lword = colorBack & 0xff};

    for (int y = 0; y < 8; y++) {
        unsigned char bitmap = fontdata_8x8[(int)c][y];
        struct pixel* c_p    = &s[y][0];

        for (int x = 8 - 1; x >= 0; x--) {
            c_p[x] = (bitmap & (1 << x)) != 0 ? color_1 : color_0;
        }
    }

    struct pixel result[8][8];

    for (int roww = 0; roww < 8; roww++) {
        for (int coll = 0; coll < 8; coll++) {
            result[roww][coll] = s[coll][8 - roww - 1];
        }
    }

    oled_memory_size(col, col + 7, row, row + 7);
    oled_send_image(&result[0][0], 8 * 8);
}

void drawer_string(const char* str,
                   int length,
                   int x,
                   int y,
                   enum colors color,
                   enum colors colorBack)
{
    for (int i = 0; i < length; i++) {
        drawer_char(str[i], x, y + 8 * i, color, colorBack);
    }
}

void drawer_rectangle(int x, int y, int w, int h, enum colors color)
{
    oled_memory_size(y, y + h - 1, x, x + w - 1);
    for (int x = y; x < y + h; x++) {
        for (int y = x; y < x + w; y++) {
            oled_color(color);
        }
    }
}

void drawer_circle(int x, int y, int r, enum colors color)
{
    int size = 0;

    for (int i = -r; i <= r; i++) {
        size = round(sqrt((r * r) - (i * i)));

        oled_memory_size(x - size, x + size, y + i, y + i);
        for (int k = 0; k <= (2 * size); k++) {
            oled_color(color);
        }
    }
}

void drawer_clear() { drawer_rectangle(0, 0, 96, 96, black); }
