#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de récupérer des valeur d'un
 *              thermomètre placé sur la carte d'extension
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */

#include <stdint.h>

struct thermo {
    int8_t cur;
    int8_t min;
    int8_t max;
};

/**
 * Mehtod to init the thermo (allow the communication with the thermo)
 */
extern void thermo_init();

/**
 * Method to get the temperature values
 * If you want to real actual current value you need to call thermo_update()
 * before
 * @return a structure that contain the min and max temperature recorder since
 * its last reset and the current temperature since its last update
 */
extern struct thermo thermo_values();

/**
 * Method to update the value of current, min and max
 */
extern void thermo_update();

/**
 * Method to reset the value of the min and max. This method call
 * thermo_update() and fix the min and max to the current temperature
 */
extern void thermo_reset();