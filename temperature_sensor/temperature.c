/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Permet d'afficher la température et l'icône du thermomètre
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        04.11.2020
 */
#include "temperature.h"

#include <stdio.h>
#include <string.h>

enum colors current_color;
enum colors min_color;
enum colors max_color;
enum colors thermometre_color;
enum colors back_color;
enum colors thermometre_color_neg_to_0;
enum colors thermometre_color_0_to_20;
enum colors thermometre_color_20_to_40;
enum colors thermometre_color_upper_40;

void temperature_init() { temperature_reset(); }

void display_icon()
{
    // Draw the thermometer
    drawer_rectangle(48, 10, 10, 2, thermometre_color);
    drawer_rectangle(48, 10, 10, 60, thermometre_color);
    drawer_circle(78, 53, 10, thermometre_color);

    // Draw the line for 40 degrees
    drawer_rectangle(40, 16, 8, 1, thermometre_color);
    // Draw the line for 20 degrees
    drawer_rectangle(40, 29, 8, 1, thermometre_color);
    // Draw the line for 0 degrees
    drawer_rectangle(40, 42, 8, 1, thermometre_color);
    // Draw the line for -20 degrees
    drawer_rectangle(40, 55, 8, 1, thermometre_color);

    // Draw the string for 40 degrees
    drawer_string("40", 2, 12, 16, thermometre_color, back_color);
    // Draw the string for 20 degrees
    drawer_string("20", 2, 25, 16, thermometre_color, back_color);
    // Draw the string for 0 degrees
    drawer_string("0", 2, 38, 24, thermometre_color, back_color);
    // Draw the string for -20 degrees
    drawer_string("-20", 3, 51, 8, thermometre_color, back_color);

    // Draw the string for maximum temperature
    drawer_string("M=", 2, 12, 65, max_color, back_color);
    // Draw the string for current temperature
    drawer_string("c=", 2, 25, 65, current_color, back_color);
    // Draw the string for minimum temperature
    drawer_string("m=", 2, 38, 65, min_color, back_color);

    // Draw rectangle for inside of thermometer
    drawer_rectangle(50, 12, 6, 68, back_color);
    // Draw cicrcle for inside of thermometer
    drawer_circle(78, 53, 8, thermometre_color_neg_to_0);
}

/**
 * Method to calculate number of pixel for temperature
 */
static int pixelsTemp(int cur)
{
    double pixels = (13.0 / 20.0) * (double)cur + 42.0;
    return (int)pixels;
}

void display_temp(int cur, int min, int max)
{
    char cur_str[8] = "";
    char min_str[8] = "";
    char max_str[8] = "";

    sprintf(cur_str, "%d", cur);
    sprintf(min_str, "%d", min);
    sprintf(max_str, "%d", max);

    // draw string for maximum temperature
    drawer_string(max_str, 2, 12, 81, max_color, back_color);
    // draw string for current temperature
    drawer_string(cur_str, 2, 25, 81, current_color, back_color);
    // draw string for minimum temperature
    drawer_string(min_str, 2, 38, 81, min_color, back_color);

    int pixels = pixelsTemp(cur);

    // Draw inside of thermometer for current temperature
    if (cur >= 40) {
        int x = pixels - 68;
        x     = x > 4 ? 4 : x;
        drawer_rectangle(50, 16 - x, 6, pixels, thermometre_color_upper_40);
        pixels = 55;
    }
    if (cur >= 20) {
        int x = pixels - 55;
        drawer_rectangle(50, 29 - x, 6, pixels, thermometre_color_20_to_40);
        pixels = 42;
    }
    if (cur >= 0) {
        int x = pixels - 42;
        drawer_rectangle(50, 42 - x, 6, pixels, thermometre_color_0_to_20);
        pixels = 29;
    }
    if (cur >= -20) {
        int x = pixels - 29;
        drawer_rectangle(50, 55 - x, 6, pixels, thermometre_color_neg_to_0);
    } else {
        drawer_rectangle(50, 12, 6, 68, back_color);
    }

    drawer_circle(78, 53, 8, thermometre_color_neg_to_0);
}

// methods to change colors
void change_color_cur(enum colors color) { current_color = color; }

void change_color_min(enum colors color) { min_color = color; }

void change_color_max(enum colors color) { max_color = color; }

// methode to reset colors
void temperature_reset()
{
    current_color              = orange;
    min_color                  = green;
    max_color                  = red;
    thermometre_color          = pink;
    back_color                 = black;
    thermometre_color_neg_to_0 = blue;
    thermometre_color_0_to_20  = green;
    thermometre_color_20_to_40 = orange;
    thermometre_color_upper_40 = red;
}
