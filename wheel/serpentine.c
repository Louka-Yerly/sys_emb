/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la
 *              carte d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include <stdint.h>
#include <stdlib.h>
#include "serpentine.h"
#include "seg7.h"
#include "wheel.h"

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))

static signed int count = 0;

static const struct serpentine {
    uint32_t left;
    uint32_t right;

} serpentinePattern[] = {  // Patern of the serpentine
    {.left = SEG7_G, .right = 0},
    {.left = SEG7_F, .right = 0},
    {.left = SEG7_A, .right = 0},
    {.left = SEG7_B, .right = 0},
    {.left = SEG7_C, .right = 0},
    {.left = SEG7_D, .right = 0},
    {.left = SEG7_E, .right = 0},
    {.left = SEG7_G, .right = 0},
    {.left = 0, .right = SEG7_G},
    {.left = 0, .right = SEG7_B},
    {.left = 0, .right = SEG7_A},
    {.left = 0, .right = SEG7_F},
    {.left = 0, .right = SEG7_E},
    {.left = 0, .right = SEG7_D},
    {.left = 0, .right = SEG7_C},
    {.left = 0, .right = SEG7_G}};

void serpentine_init() { serpentine_reset(); }

// method to call the seg7 interface in order to display the segment
static void serpentine_display()
{
    seg7_display_dots(SEG7_LEFT, 0);
    seg7_display_dots(SEG7_RIGHT, 0);
    seg7_display_segments(SEG7_LEFT, serpentinePattern[count].left);
    seg7_display_segments(SEG7_RIGHT, serpentinePattern[count].right);
}

void serpentine_process(enum wheel_directions direction)
{
    if (direction == WHEEL_LEFT) {
        count++;
    } else if (direction == WHEEL_RIGHT) {
        count--;
    }
    count =
        (count + ARRAY_SIZE(serpentinePattern)) % ARRAY_SIZE(serpentinePattern);

    serpentine_display();
}

void serpentine_reset() { count = 0; }