/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la carte
 *              d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include "buttons.h"

#include <bbb/am335x_gpio.h>

#define BUTTONS_GPIO AM335X_GPIO1
#define BUTTON1 15
#define BUTTON2 16
#define BUTTON3 17

#define SHIFT BUTTON1
#define MASK (BUTTONS_1 | BUTTONS_2 | BUTTONS_3)

void buttons_init()
{
    am335x_gpio_init(BUTTONS_GPIO);

    am335x_gpio_setup_pin_in(
        BUTTONS_GPIO, BUTTON1, AM335X_GPIO_PULL_NONE, false);
    am335x_gpio_setup_pin_in(
        BUTTONS_GPIO, BUTTON2, AM335X_GPIO_PULL_NONE, false);
    am335x_gpio_setup_pin_in(
        BUTTONS_GPIO, BUTTON3, AM335X_GPIO_PULL_NONE, false);
}

uint32_t buttons_state()
{
    uint32_t state = am335x_gpio_get_states(BUTTONS_GPIO);
    state          = (~state >> SHIFT) & MASK;
    return state;
}
