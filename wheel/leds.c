/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la carte
 *              d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include "leds.h"

#include <bbb/am335x_gpio.h>

#define LED_GPIO AM335X_GPIO1
#define LED1 12
#define LED2 13
#define LED3 14

#define SHIFT LED1
#define MASK (LEDS_1 | LEDS_2 | LEDS_3)

void leds_init()
{
    am335x_gpio_init(LED_GPIO);

    am335x_gpio_setup_pin_out(LED_GPIO, LED1, false);
    am335x_gpio_setup_pin_out(LED_GPIO, LED2, false);
    am335x_gpio_setup_pin_out(LED_GPIO, LED3, false);
}

void leds_set_state(uint32_t state)
{
    am335x_gpio_change_states(LED_GPIO, (~state & MASK) << SHIFT, false);
    am335x_gpio_change_states(LED_GPIO, (state & MASK) << SHIFT, true);
}
