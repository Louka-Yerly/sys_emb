/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la
 *              carte d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#include "buttons.h"
#include "counter.h"
#include "leds.h"
#include "seg7.h"
#include "serpentine.h"
#include "wheel.h"

static const char* banner =
    "HEIA-FR - Embedded Systems 1 Laboratory\n"
    "An introduction the C programming language\n"
    "--> 7-segment and wheel demo program\n";

enum modes { COUNTER, SERPENTINE };


//-- implementation of public methods ---------------------------------------

int main()
{
    // display banner on the console
    printf("%s", banner);

    // initialize the drivers
    leds_init();
    buttons_init();
    wheel_init();
    seg7_init();

    // initialize the 2 modes
    serpentine_init();
    counter_init();

    // default mode when starting
    enum modes mode = COUNTER;

    // main loop
    while (true) {
        enum leds_set leds_state = 0;
        // get state of the buttons
        uint32_t state = buttons_state();

        // get the wheel direction
        enum wheel_directions direction = wheel_direction();

        // change mode on button state changed
        if ((state & BUTTONS_3) != 0) {
            leds_state |= LEDS_3;
            counter_reset();
            serpentine_reset();
        } else if ((state & BUTTONS_1) != 0) {
            mode = COUNTER;
        } else if ((state & BUTTONS_2) != 0) {
            mode = SERPENTINE;
        } else {
            // The mode hasn't changed (mode = mode)
        }

        // process the game according to the mode
        if (mode == COUNTER) {
            leds_state |= LEDS_1;
            counter_process(direction);
        } else if (mode == SERPENTINE) {
            leds_state |= LEDS_2;
            serpentine_process(direction);
        } else {
            // there is no other mode
        }

        // set the state of the led
        leds_set_state(leds_state);

        // actualize the seg7
        seg7_refresh();
    }
    return 0;
}