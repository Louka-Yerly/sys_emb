#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la carte
 *              d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include <stdint.h>

enum leds_set {
    LEDS_1 = (1 << 0),
    LEDS_2 = (1 << 1),
    LEDS_3 = (1 << 2),
};

/**
 * method to initialize the leds
 */
extern void leds_init();

/**
 * methode to set the states of the leds
 * @param state : state of the leds desired
 */
extern void leds_set_state(uint32_t state);
