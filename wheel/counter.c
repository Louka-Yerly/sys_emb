/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la carte
 *              d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */


#include <stdint.h>
#include "counter.h"
#include "seg7.h"
#include "wheel.h"

/* 7-segment: segment definition ---------------------------------------------

    DP1 o  +----(A)----+
           |           |
          (F)          (B)
           |           |
           +----(G)----+
           |           |
          (E)         (C)
           |           |
           +----(D)----+ o DP2

*/

// tab of numbers 0-9
static const uint32_t digit_number[] = {
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_E + SEG7_F),           // 0
    (SEG7_B + SEG7_C),                                               // 1
    (SEG7_A + SEG7_B + SEG7_D + SEG7_E + SEG7_G),                    // 2
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_G),                    // 3
    (SEG7_B + SEG7_C + SEG7_F + SEG7_G),                             // 4
    (SEG7_A + SEG7_C + SEG7_D + SEG7_F + SEG7_G),                    // 5
    (SEG7_A + +SEG7_C + SEG7_D + SEG7_E + SEG7_F + SEG7_G),          // 6
    (SEG7_A + SEG7_B + SEG7_C),                                      // 7
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_E + SEG7_F + SEG7_G),  // 8
    (SEG7_A + SEG7_B + SEG7_C + SEG7_D + SEG7_F + SEG7_G),           // 9
};

// counter for number -99 to 99
static int counter = 0;

void counter_init() { counter = 0; }

/**
 * method to send information to display to 7 segments
 * @param value : number to display
 **/
static void number_display(int value)
{
    uint32_t dot = 0;
    if (value < 0) {
        dot   = SEG7_DP1;
        value = -value;
    }

    seg7_display_dots(SEG7_LEFT, dot);
    seg7_display_dots(SEG7_RIGHT, 0);
    seg7_display_segments(SEG7_RIGHT, digit_number[value % 10]);
    seg7_display_segments(SEG7_LEFT, digit_number[value / 10]);
}

void counter_process(enum wheel_directions direction)
{
    if (direction == WHEEL_LEFT) {
        if (counter < 99) counter++;
    } else if (direction == WHEEL_RIGHT) {
        if (counter > -99) counter--;
    } else {
        // nothing to do WHEEL_STILL
    }
    number_display(counter);
}

void counter_reset() { counter = 0; }
