/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la
 *              carte d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include "seg7.h"

#include <bbb/am335x_gpio.h>
#include <stdbool.h>
#include <stdint.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof(x[0]))
#define INIT false

#define GPIO_0 AM335X_GPIO0
#define GPIO_2 AM335X_GPIO2

#define DIG1_PIN 2
#define DIG2_PIN 3
#define DIG1_LEFT (1 << DIG1_PIN)
#define DIG2_RIGHT (1 << DIG2_PIN)
#define DIG_ALL (DIG1_LEFT | DIG2_RIGHT)

#define DP1_PIN 4
#define DP2_PIN 5
#define DP1_LEFT (1 << DP1_PIN)
#define DP2_RIGHT (1 << DP2_PIN)
#define DP_ALL (DP1_LEFT | DP2_RIGHT)

#define SEG_A_PIN 4
#define SEG_B_PIN 5
#define SEG_C_PIN 14
#define SEG_D_PIN 22
#define SEG_E_PIN 23
#define SEG_F_PIN 26
#define SEG_G_PIN 27
#define SEG_A (1 << SEG_A_PIN)
#define SEG_B (1 << SEG_B_PIN)
#define SEG_C (1 << SEG_C_PIN)
#define SEG_D (1 << SEG_D_PIN)
#define SEG_E (1 << SEG_E_PIN)
#define SEG_F (1 << SEG_F_PIN)
#define SEG_G (1 << SEG_G_PIN)
#define SEG_ALL (SEG_A | SEG_B | SEG_C | SEG_D | SEG_E | SEG_F | SEG_G)

// current values of the left/right segments
static uint32_t display_left  = 0;
static uint32_t display_right = 0;

// current values of the left/right dp
static uint32_t dp_left  = 0;
static uint32_t dp_right = 0;

// current digit
static bool is_left = false;

struct gpio {
    enum am335x_gpio_modules gpio_nr;
    uint32_t pin_nr;
    bool state;
};
static const struct gpio gpio_SEG[] = {
    {GPIO_0, SEG_A_PIN, INIT},  // SEG_A
    {GPIO_0, SEG_B_PIN, INIT},  // SEG_B
    {GPIO_0, SEG_C_PIN, INIT},  // SEG_C
    {GPIO_0, SEG_D_PIN, INIT},  // SEG_D
    {GPIO_0, SEG_E_PIN, INIT},  // SEG_E
    {GPIO_0, SEG_F_PIN, INIT},  // SEG_F
    {GPIO_0, SEG_G_PIN, INIT},  // SEG_G
};
static const struct gpio gpio_DOT[] = {
    {GPIO_2, DP1_PIN, INIT},  // DP1 (LEFT)
    {GPIO_2, DP2_PIN, INIT},  // DP2 (RIGHT)
};
static const struct gpio gpio_DIG[] = {
    {GPIO_2, DIG1_PIN, INIT},  // DIG1 (LEFT)
    {GPIO_2, DIG2_PIN, INIT},  // DIG2 (RIGHT)
};

void seg7_init()
{
    // init the GPIO
    am335x_gpio_init(GPIO_2);
    am335x_gpio_init(GPIO_0);

    // setup pin out
    for (int i = ARRAY_SIZE(gpio_SEG) - 1; i >= 0; i--) {
        am335x_gpio_setup_pin_out(
            gpio_SEG[i].gpio_nr, gpio_SEG[i].pin_nr, gpio_SEG[i].state);
    }
    for (int i = ARRAY_SIZE(gpio_DOT) - 1; i >= 0; i--) {
        am335x_gpio_setup_pin_out(
            gpio_DOT[i].gpio_nr, gpio_DOT[i].pin_nr, gpio_DOT[i].state);
    }
    for (int i = ARRAY_SIZE(gpio_DIG) - 1; i >= 0; i--) {
        am335x_gpio_setup_pin_out(
            gpio_DIG[i].gpio_nr, gpio_DIG[i].pin_nr, gpio_DIG[i].state);
    }
}

// map the software value with the hardware value for the segment(s)
static uint32_t mappageSEG(uint32_t value)
{
    uint32_t result = 0;
    for (unsigned int i = 0; i < ARRAY_SIZE(gpio_SEG); i++) {
        if ((value & (1 << i)) != 0) {
            result |= 1 << gpio_SEG[i].pin_nr;
        }
    }
    return result;
}

// map the software value with the hardware value for the dot(s)
static uint32_t mappageDOT(uint32_t value)
{
    uint32_t result = 0;
    for (unsigned int i = 0; i < ARRAY_SIZE(gpio_DOT); i++) {
        if ((value & (1 << i)) != 0) {
            result |= 1 << gpio_DOT[i].pin_nr;
        }
    }
    return result;
}

void seg7_display_segments(enum seg7_display display, uint32_t value)
{
    uint32_t segments = mappageSEG(value);
    if (display == SEG7_LEFT) {
        display_left = segments;
    } else {
        display_right = segments;
    }
}

void seg7_display_dots(enum seg7_display display, uint32_t value)
{
    uint32_t dots = mappageDOT(value);
    if (display == SEG7_LEFT) {
        dp_left = dots;
    } else {
        dp_right = dots;
    }
}

void seg7_refresh()
{
    // clear all segments and points
    am335x_gpio_change_states(GPIO_0, SEG_ALL, false);
    am335x_gpio_change_states(GPIO_2, DP_ALL, false);
    am335x_gpio_change_states(GPIO_2, DIG_ALL, false);

    // set on the segment(s)
    int curr_seg = is_left ? display_left : display_right;
    am335x_gpio_change_states(GPIO_0, curr_seg, true);

    // set on the point(s)
    int curr_dp = is_left ? dp_left : dp_right;
    am335x_gpio_change_states(GPIO_2, curr_dp, true);

    // write on the current digit
    int curr_dig = is_left ? DIG1_PIN : DIG2_PIN;
    am335x_gpio_change_state(GPIO_2, curr_dig, true);

    is_left = !is_left;
}