#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la carte
 *              d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include <stdint.h>

/**
 * software position of the 3 buttons
 * */
enum buttons_set {
    BUTTONS_1 = (1 << 0),
    BUTTONS_2 = (1 << 1),
    BUTTONS_3 = (1 << 2),
};

/**
 * method to initialize the buttons
 */
extern void buttons_init();

/**
 * method to get the state of the 3 buttons
 * @return in the first three positions the state of the 3 buttons (1=pressed,
 * 0=released)
 */
extern uint32_t buttons_state();
