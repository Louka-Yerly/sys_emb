#pragma once
/**
 * Copyright 2020 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project:     HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract:    Introduction à la programmation modulaire en C
 *
 * Purpose:     Concevoir un programme capable de piloter l'encodeur rotatif,
 *              les boutons poussoir, les LEDs et l'afficheur 7-segment de la
 *              carte d'extension HEIA-FR
 *
 * Author:      Bryan Haymoz & Louka Yerly
 * Date:        30.09.2020
 */

#include <stdint.h>

enum seg7_display {
    SEG7_RIGHT,
    SEG7_LEFT,
};

/* structure to initialize gpio pins used by 7-segment
   segment definition

           +-- SEG_A --+
           |           |
         SEG_F       SEG_B
           |           |
           +-- SEG_G --+
           |           |
         SEG_E       SEG_C
           |           |
           +-- SEG_D --+
*/

enum seg7_segments {
    SEG7_A   = (1 << 0),
    SEG7_B   = (1 << 1),
    SEG7_C   = (1 << 2),
    SEG7_D   = (1 << 3),
    SEG7_E   = (1 << 4),
    SEG7_F   = (1 << 5),
    SEG7_G   = (1 << 6),
    SEG7_ALL = (SEG7_A | SEG7_B | SEG7_C | SEG7_D | SEG7_E | SEG7_F | SEG7_G)
};

enum seg7_dots {
    SEG7_DP1    = (1 << 0),
    SEG7_DP2    = (1 << 1),
    SEG7_DP_ALL = (SEG7_DP1 | SEG7_DP2)
};

/**
 * method to initialize the seg7
 */
extern void seg7_init();

/**
 * method to set the segments to display
 * @param display : the desired dispay (SEG7_LEFT, SEG7_RIGHT)
 * @param value : the segments to enable according to the enum seg7_segments
 */
extern void seg7_display_segments(enum seg7_display display, uint32_t value);

/**
 * methode to set the dots to display
 * @param display : the desired display (SEG7_LEFT, SEG7_RIGHT)
 * @param value : the dots to enable according to the enum seg7_dots
 */
extern void seg7_display_dots(enum seg7_display display, uint32_t value);

/**
 * method to refresh the seg7 to actualize the digits
 */
extern void seg7_refresh();
