/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract: Introduction the C programming language
 *
 * Purpose: Game tower of hanoi.
 *
 * Author:  Bryan Haymoz & Louka Yerly
 * Date:    21.12.2020
 */

CANVAS0_DEG = 0

DISPLAY_BLACK = 0x0000
DISPLAY_WHITE = 0xffff
DISPLAY_YELLOW = 0xf7a0
DISPLAY_RED = 0xf800
DISPLAY_GREEN = 0x07e0
DISPLAY_BLUE = 0x001f


NUMBER_OF_PEGS = 3
TOWER_HEIGHT = 10
MARGIN_X = 1
MARGIN_Y = 32

PEG_WIDTH = 2
PEG_HEIGHT = 36
PEG_DISTANCE = 32
PEG_COLOR = DISPLAY_WHITE

BASE_WIDTH = 28
BASE_HEIGHT = 2
BASE_COLOR = DISPLAY_WHITE

DISK_HEIGHT = 2
DISK_SPACING = 1
DISK_DISTANCE = (DISK_HEIGHT+DISK_SPACING)



.section .rodata
.align 2
disk_color: .short  DISPLAY_BLUE
            .short  DISPLAY_GREEN
            .short  DISPLAY_RED
            .short  DISPLAY_YELLOW
            .short  DISPLAY_WHITE
            .short  DISPLAY_BLUE
            .short  DISPLAY_GREEN
            .short  DISPLAY_RED
            .short  DISPLAY_YELLOW
            .short  DISPLAY_WHITE

welcome: .asciz " Welcome to "
hanoi: .asciz   "   HANOI    "
merry: .asciz   "Merry X-Mas!"
newYear: .asciz ":) new(Year)"

.section .bss
.align 2
//static uint8_t heigh[NUMBER_OF_PEGS];
disk_height: .byte 0x0
             .byte 0x0
             .byte 0x0



.section .text
.align 2
//static void clear_disk(int peg, int height);
clear_disk: nop
            push    {r4-r11, lr}
            mov     r4, r0      // peg
            mov     r5, r1      // height

            // r0
            ldr     r0, =PEG_DISTANCE   // r0 = PEG_DISTANCE
            mul     r0, r4, r0          // r0 = peg*PEG_DISTANCE
            add     r0, #MARGIN_X       // r0 = r0 + MARGIN_X

            // r1
            ldr     r1, =MARGIN_Y       // r1 = MARGIN_Y
            add     r1, #BASE_HEIGHT    // r1 = r1 + BASE_HEIGHT
            add     r1, #DISK_SPACING   // r1 = r1 + DISK_SPACING
            ldr     r2, =TOWER_HEIGHT   // r2 = TOWER_HEIGHT
            sub     r2, r5              // r2 = r2 - height
            ldr     r3, =DISK_DISTANCE  // r3 = DISK_DISTANCE
            mul     r2, r2, r3          // r2 = r2*r3
            add     r1, r2              // r1 = r1 + r2

            // r2
            ldr     r2, =BASE_WIDTH     // r2 = BASE_WIDTH
            
            // r3
            ldr     r3, =DISK_HEIGHT    // r3 = DISK_HEIGHT
            
            // the last parameter on stack
            sub     sp, #4
            ldr     r6, =DISPLAY_BLACK  // r6 = DISPLAY_BLACK
            str     r6, [sp]            // r6 on stack

            // clear the disk
            bl      canvas_rectangle_f
            add     sp, #4              // reserve space

            // r0
            ldr     r0, =PEG_DISTANCE   // r0 = PEG_DISTANCE
            mul     r0, r4, r0          // r0 = peg*PEG_DISTANCE
            add     r0, #MARGIN_X       // r0 = r0 + MARGIN_X
            ldr     r1, =BASE_WIDTH     // r1 = BASE_WIDTH
            add     r0, r0, r1, LSR #1  // r0 = r0 + r1/2
            ldr     r1, =PEG_WIDTH      // r1 = PEG_WIDTH
            sub     r0, r0, r1, LSR #1  // r0 = r0 + r1/2

            // r1
            ldr     r1, =MARGIN_Y       // r1 = MARGIN_Y
            add     r1, #BASE_HEIGHT    // r1 = r1 + BASE_HEIGHT
            add     r1, #DISK_SPACING   // r1 = r1 + DISK_SPACING
            ldr     r2, =TOWER_HEIGHT   // r2 = DISK_SPACING
            sub     r2, r5              // r2 = r2 - height
            ldr     r3, =DISK_DISTANCE  // r3 = DISK_DISTANCE
            mul     r2, r2, r3          // r2 = r2*r3
            add     r1, r2              // r1 = r1 + r2

            // r2
            ldr     r2, =PEG_WIDTH      // r2 = BASE_WIDTH
            
            // r3
            ldr     r3, =DISK_HEIGHT    // r3 = DISK_HEIGHT
            
            // the last parameter on stack
            sub     sp, #4
            ldr     r6, =PEG_COLOR      // r6 = PEG_COLOR
            str     r6, [sp]            // r6 on stack

            // redraw the missing part of the peg
            bl      canvas_rectangle_f
            add     sp, #4              // delete the space

            // refresh
            bl      canvas_refresh

            pop     {r4-r11, pc}
            bx      lr



//static void draw_disk(int peg, int height, int disk);
draw_disk:  nop
            push    {r4-r11, lr}
            mov     r4, r0 // peg
            mov     r5, r1 // height
            mov     r6, r2 // disk


            // r0
            ldr     r0, =PEG_DISTANCE   // r0 = PEG_DISTANCE
            mul     r0, r4, r0          // r0 = peg*PEG_DISTANCE
            add     r0, #MARGIN_X       // r0 = r0 + MARGIN_X
            ldr     r1, =BASE_WIDTH     // r1 = BASE_WIDTH
            add     r0, r0, r1, LSR #1  // r0 = r0 + r1/2
            ldr     r1, =PEG_WIDTH      // r4 = PEG_WIDTH
            sub     r0, r0, r1, LSR #1  // r0 = r0 - r1/2
            add     r1, r6, #1          // r4 = disk + 1
            sub     r0, r1              // r0 = r0 - r1
            
            // r1
            ldr     r1, =MARGIN_Y       // r1 = MARGIN_Y
            add     r1, #BASE_HEIGHT    // r1 = r1 + BASE_HEIGHT
            add     r1, #DISK_SPACING   // r1 = r1 + DISK_SPACING
            ldr     r2, =TOWER_HEIGHT   // r2 = TOWER_HEIGHT
            sub     r2, r5              // r2 = r2 - height
            ldr     r3, =DISK_DISTANCE  // r3 = DISK_DISTANCE
            mul     r2, r2, r3          // r2 = r2*r3
            add     r1, r2              // r1 = r1 + r2

            // r2
            ldr     r2, =PEG_WIDTH      // r2 = PEG_WIDTH
            mov     r3, r6              // r3 = disk
            add     r3, #1              // r3 = r3 + 1
            add     r2, r2, r3, LSL #1  // r5 = r5 + r3*2

            // r3
            ldr r3, =DISK_HEIGHT // r6 = DISK_HEIGHT
            
            //r7
            ldr     r7, =disk_color // r7 = &disk_color
            lsl     r8, r6, #1      // r8 = r6*2
            ldrsh   r8, [r7, +r8]   // r8 = disk_color[disk] 
            sub     sp, #4          // reserve space
            str     r8, [sp]        // r8 on stack

            bl      canvas_rectangle_f
            add     sp, #4          // delete the space

            bl      canvas_refresh

            pop     {r4-r11, pc}
            bx      lr          

// static void push_disk(int peg, int disk);
push_disk:  nop
            push    {r4-r11, lr}
            ldr     r4, =disk_height    // r4 = &disk_height
            ldrb    r5, [r4, +r0]!      // r4 = r4 + peg     r5 = disk_height[peg]
            mov     r2, r1              // r2 = r1
            mov     r1, r5              // r1 = r5

            bl      draw_disk
            add     r5, #1      // r5 = r5 + 1 (disk_height++)
            strb    r5, [r4]    // store disk_height
            pop     {r4-r11, pc}
            bx      lr



// static void move_disk(int from, int to, int disk);
move_disk:  nop
            push    {r4-r11, lr}
            mov     r4, r0  // from
            mov     r5, r1  // to
            mov     r6, r2  // disk
            ldr     r7, =disk_height    // r4 = &disk_height
            ldrb    r8, [r7, +r4]!      // r7 = &disk_height + peg    r8 = disk_height[peg] 
            sub     r8, #1              // disk_height[peg]--
            strb    r8, [r7]            // store disk_height[peg]

            mov     r0, r4      // r0 = from
            mov     r1, r8      // r1 = disk_height[peg]
            bl      clear_disk

            mov     r0, r5      // r0 = to
            mov     r1, r6      // r1 = disk
            bl      push_disk

            pop     {r4-r11, pc}
            bx      lr

// static void draw_peg(int peg);
draw_peg:   nop
            push    {r4-r11, lr}
            mov     r4, r0  // peg

            // r0
            ldr     r0, =PEG_DISTANCE   //  r0 = PEG_DISTANCE 
            mul     r0, r4, r0          //  r0 = peg*PEG_DISTANCE
            add     r0, #MARGIN_X       //  r0 = r0 + MARGIN_X

            // r1
            ldr     r1, =MARGIN_Y       // r1 = MARGIN_Y
            add     r1, #PEG_HEIGHT     // r1 = r1 + PEG_HEIGHT
            
            // r2
            ldr     r2, =BASE_WIDTH     // r2 = BASE_WIDTH

            // r3
            ldr     r3, =BASE_HEIGHT    // r3 = BASE_HEIGHT

            // the last on the stack
            sub     sp, #4              // reserve space
            ldr     r5, =BASE_COLOR     // r5 = BASE_COLOR
            str     r5, [sp]            // r5 on the stack

            bl canvas_rectangle_f
            add     sp, #4              // delete the space

            // r0
            ldr     r0, =PEG_DISTANCE   // r0 = PEG_DISTANCE
            mul     r0, r4, r0          // r0 = peg*PEG_DISTANCE
            add     r0, #MARGIN_X       // r0 = r0 + MARGIN_X
            ldr     r1, =BASE_WIDTH     // r1 = BASE_WIDTH 
            add     r0, r0, r1, LSR #1  // r0 = r0 + BASE_WIDTH/2
            ldr     r1, =PEG_WIDTH      // r1 = PEG_WIDTH
            sub     r0, r0, r1, LSR #1  // r0 = r0 + PEG_WIDTH/2

            // r1
            ldr     r1, =MARGIN_Y   // r1 = MARGIN_Y

            // r2
            ldr     r2, =PEG_WIDTH  // r2 = PEG_WIDTH

            // r3
            ldr     r3, =PEG_HEIGHT // r3 = PEG_HEIGHT

            // the last on the stack
            sub     sp, #4          // reserve space
            ldr     r5, =PEG_COLOR  // r5 = PEG_COLOR
            str     r5, [sp]        // r5 on the stack


            bl      canvas_rectangle_f
            add     sp, #4              // delete the space

            bl      canvas_refresh

            pop     {r4-r11, pc}
            bx      lr


// void tower_of_hanoi_init(int peg)
.global tower_of_hanoi_init
tower_of_hanoi_init:    nop
                        push    {r4-r11, lr}
                        mov     r4, r0
                        ldr     r0, =CANVAS0_DEG    // r0 = CANVAS0_DEG
                        bl      canvas_init

                        ldr     r0, =DISPLAY_BLACK  // r0 = DISPLAY_BLACK 
                        bl      canvas_clear

                        ldr     r5, =NUMBER_OF_PEGS-1   // r5 = NUMBER_OF_PEGS-1
1:                      mov     r0, r5                  // r0 = r5
                        bl      draw_peg
                        subs    r5, #1                  // update the flags
                        bhs     1b                      // r5>=0 ? 

                        ldr     r5, =TOWER_HEIGHT-1 // r5 = TOWER_HEIGHT
2:                      mov     r0, r4              // r0 = peg
                        mov     r1, r5              // r1 = r5
                        bl      push_disk
                        subs    r5, #1              // update the flags
                        bhs     2b                  // r5>=0 ?

                        ldr     r0, =0              // r0 = 0
                        ldr     r1, =0*8            // r1 = 0*8
                        ldr     r2, =welcome        // r2 = &" Welcome to "
                        ldr     r3, =DISPLAY_WHITE  // r3 = DISPLAY_WHITE
                        bl      canvas_text

                        ldr     r0, =0              // r0 = 0
                        ldr     r1, =2*8            // r1 = 2*8
                        ldr     r2, =hanoi          // r2 = &"   HANOI    "
                        ldr     r3, =DISPLAY_WHITE  // r3 = DISPLAY_WHITE
                        bl      canvas_text

                        ldr     r0, =0              // r0 = 0
                        ldr     r1, =9*8            // r1 = 9*8
                        ldr     r2, =merry          // r2 = &"Merry X-Mas!"
                        ldr     r3, =DISPLAY_YELLOW // r3 = DISPLAY_YELLOW
                        bl      canvas_text

                        ldr     r0, =0              // r0 = 0
                        ldr     r1, =11*8           // r1 = 11*8
                        ldr     r2, =newYear        // r2 = &":) new(Year)"
                        ldr     r3, =DISPLAY_RED    // r3 = DISPLAY_RED
                        bl      canvas_text

                        bl      canvas_refresh
                        pop     {r4-r11, pc}
                        bx      lr

// void tower_of_hanoi_move(int from, int to, int by, int height)
.global tower_of_hanoi_move
tower_of_hanoi_move:    nop
                        push    {r4-r11, lr}
                        cmp     r3, #0
                        ble     1f
                        mov     r4, r0      // from
                        mov     r5, r1      // to
                        mov     r6, r2      // by
                        sub     r7, r3, #1  // height

                        mov     r0, r4      // r0 = from
                        mov     r1, r6      // r1 = by
                        mov     r2, r5      // r2 = to
                        mov     r3, r7      // r3 = height-1
                        bl      tower_of_hanoi_move

                        mov     r0, r4      // r0 = from
                        mov     r1, r5      // r1 = to
                        mov     r2, r7      // r2 = height-1
                        bl      move_disk

                        mov     r0, r6      // r0 = by
                        mov     r1, r5      // r1 = to
                        mov     r2, r4      // r2 = from
                        mov     r3, r7      // r3 = height-1
                        bl      tower_of_hanoi_move

1:                      pop    {r4-r11, pc}
                        bx      lr

// int tower_of_hanoi_disks()
.global tower_of_hanoi_disks
tower_of_hanoi_disks:   nop
                        ldr     r0, =TOWER_HEIGHT // r0 = TOWER_HEIGHT (return value)
                        bx      lr