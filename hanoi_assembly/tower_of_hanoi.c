/**
 * Copyright 2018 University of Applied Sciences Western Switzerland / Fribourg
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Project: HEIA-FR / Embedded Systems 1 Laboratory
 *
 * Abstract: Introduction the C programming language
 *
 * Purpose: Game tower of hanoi.
 *
 * Author:  
 * Date:    
 */

#include "tower_of_hanoi.h"
#include "canvas/canvas.h"


#define DISPLAY_BLACK 0x0000
#define DISPLAY_WHITE 0xffff
#define DISPLAY_YELLOW 0xf7a0
#define DISPLAY_RED 0xf800
#define DISPLAY_GREEN 0x07e0
#define DISPLAY_BLUE 0x001f


#define NUMBER_OF_PEGS 3
#define TOWER_HEIGHT 10
#define MARGIN_X 1
#define MARGIN_Y 32

#define PEG_WIDTH 2
#define PEG_HEIGHT 36
#define PEG_DISTANCE 32
#define PEG_COLOR DISPLAY_WHITE

#define BASE_WIDTH 28
#define BASE_HEIGHT 2
#define BASE_COLOR DISPLAY_WHITE

#define DISK_HEIGHT 2
#define DISK_SPACING 1
#define DISK_DISTANCE (DISK_HEIGHT+DISK_SPACING)


static color_t disk_color[TOWER_HEIGHT] = {
    DISPLAY_BLUE,
    DISPLAY_GREEN,
    DISPLAY_RED,
    DISPLAY_YELLOW,
    DISPLAY_WHITE,
    DISPLAY_BLUE,
    DISPLAY_GREEN,
    DISPLAY_RED,
    DISPLAY_YELLOW,
    DISPLAY_WHITE
};

static uint8_t heigh[NUMBER_OF_PEGS];

/**
 * method to clear the disk located at position "height" out of the given "peg"
 */
static void clear_disk(int peg, int height){
    //Clear the disk
    canvas_rectangle_f(MARGIN_X + peg * PEG_DISTANCE, 
                        MARGIN_Y + BASE_HEIGHT + DISK_SPACING + (TOWER_HEIGHT - height) * DISK_DISTANCE, 
                        BASE_WIDTH,
                        DISK_HEIGHT, 
                        DISPLAY_BLACK);
    //Redraw the missing part of the peg 
    canvas_rectangle_f(MARGIN_X + BASE_WIDTH / 2 + peg * PEG_DISTANCE - PEG_WIDTH / 2,
                        MARGIN_Y + BASE_HEIGHT + DISK_SPACING + (TOWER_HEIGHT - height) * DISK_DISTANCE,
                        PEG_WIDTH,
                        DISK_HEIGHT,
                        PEG_COLOR);
    canvas_refresh();
}

/**
 * method to draw a "disk" on given "peg" at the specified position "height"
 */
static void draw_disk(int peg, int height, int disk){
    canvas_rectangle_f(MARGIN_X + peg * PEG_DISTANCE + BASE_WIDTH / 2 - PEG_WIDTH / 2 - (disk + 1),
                    MARGIN_Y + BASE_HEIGHT + DISK_SPACING + (TOWER_HEIGHT - height) * DISK_DISTANCE,
                    PEG_WIDTH + 2 * (disk + 1),
                    DISK_HEIGHT,
                    disk_color[disk]);
    canvas_refresh();
}

/**
 * method to push a "disk" onto the given "peg"
 */
static void push_disk(int peg, int disk){
    draw_disk(peg, heigh[peg]++, disk);
}

/**
 * method to move a "disk" out of specified peg "from" to another one "to"
 */
static void move_disk(int from, int to, int disk){
    clear_disk(from, --heigh[from]);
    push_disk(to, disk);
}

/**
 * method to draw a peg
 */
static void draw_peg(int peg){
    canvas_rectangle_f(
        MARGIN_X + peg*PEG_DISTANCE,
        MARGIN_Y + PEG_HEIGHT,
        BASE_WIDTH,
        BASE_HEIGHT,
        BASE_COLOR);

    canvas_rectangle_f(
        MARGIN_X + BASE_WIDTH/2 + peg*PEG_DISTANCE - PEG_WIDTH/2,
        MARGIN_Y,
        PEG_WIDTH,
        PEG_HEIGHT,
        PEG_COLOR);

    canvas_refresh();
}

void tower_of_hanoi_init(int peg)
{
    canvas_init(CANVAS_0_DEG);
    canvas_clear(DISPLAY_BLACK);

	for (int i = NUMBER_OF_PEGS-1; i>=0; i--) {
		draw_peg(i);
	}

	for (int i = TOWER_HEIGHT-1; i >= 0; i--) {
		push_disk(peg, i);
	}

	canvas_text(0, 0*8, " Welcome to ", DISPLAY_WHITE);
	canvas_text(0, 2*8, "   HANOI    ", DISPLAY_WHITE);

	canvas_text(0, 9*8, "Merry X-Mas!", DISPLAY_YELLOW);
	canvas_text(0,11*8, ":) new(Year)", DISPLAY_RED);

    canvas_refresh();
}

void tower_of_hanoi_move(int from, int to, int by, int height)
{
	if (height > 0) {
		height--;
		tower_of_hanoi_move(from, by, to, height);
		move_disk(from, to, height);
		tower_of_hanoi_move(by, to, from, height);
	}
} 

int tower_of_hanoi_disks(){
    return TOWER_HEIGHT;
}


